---
title: "Contribute"
date: 2022-09-18
draft: false
tags:
- bitcoin
- gitlab
weight: 100

---

The best way to support this project is to share and discuss it with people you
know.

# Feedback

* Please share any feedback using the `Suggest Changes` link at the top of
  every post, or by
  [creating an issue on gitlab](https://gitlab.com/deprecatetheweb/freetheinter.net/-/issues/new).

# Contribute directly

* Work toward implementing [the components](/posts/components/).
* [Define, detail, or refine components](https://gitlab.com/deprecatetheweb/freetheinter.net/-/tree/main/content/posts/components).

# Fund development

For now this project is just a hobby. To incentivize more development time,
please donate to this bitcoin address:

[![bitcoin:bc1qdst870xmlgrl7wtp473sljufhged0gy22wga2flh0kqcv244rylqp5xpfq](/donate.png)](bitcoin:bc1qdst870xmlgrl7wtp473sljufhged0gy22wga2flh0kqcv244rylqp5xpfq)

Any transaction spent from donated funds will be documented here
with an explanation of purpose. Donations may be used to incentivize work on
[the components](/posts/components/) or for other purposes related to the project's
goals.
