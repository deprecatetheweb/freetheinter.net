---
draft: false
title: Motivation
date: 2022-07-01
tags:
- cta
- consensus
---

**Humans have lost control of the future.**

**We can regain control by facilitating widespread consensus.**

<!--more-->

Most information comes from centralized sources which are heavily
biased according to the will of their owners. This includes advertising,
content promotion, filtering, moderation, etc. All of this activity
is self-serving -- used not only to sell products, but also to control
the flow of information in our society.

The summed cost of this manipulation is too great to
ignore. People shouldn't have to
tolerate this intrusiveness in order to simply communicate, coordinate, and
collaborate online.

The internet grants us the ability to connect, communicate, and organize
in uncountably many ways, yet the vast majority of interactions
rely on untrustworthy centralized services.

It's time to rebuild the centralized social networks in a way that serves
only the interests of individuals.

For this project, we will try to find the best way for
individuals to take direct ownership of information, circumventing the
channels that have been abused by centralized entities to manipulate and
divide us.
