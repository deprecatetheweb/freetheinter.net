---
title: "Messaging"
date: 2022-12-11
draft: false
showtoc: true
tocopen: true
weight: 60
tags:
- component

---

Simple messaging layer, which can be built upon for more complex communication
schemes.

* sharing of simple messages, with references to other messages
* creation of conversation groups
* group message exchange between clusters
