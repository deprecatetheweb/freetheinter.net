---
title: "Data"
date: 2022-12-11
draft: false
showtoc: true
tocopen: true
weight: 65
tags:
- component

---

Data layer - persistent storage duplicated between one's own nodes and some
nodes of trusted friends.

* stores messages
* always encrypted at rest
* combinatoric streaming engine requires compromise of multiple nodes to
  access archived data. combine stream of two remote nodes to construct
  messages in memory locally
* redundant - any two nodes can be used to recover all data
* relay, sender, receiver roles
* trusted friends can serve relay role
* only one sender at a time
* fully encrypted on
  nodes of trusted people you know
