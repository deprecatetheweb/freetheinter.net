---
title: "Clustering"
date: 2022-12-11
draft: false
showtoc: true
tocopen: true
weight: 51
tags:
- component

---

Functional goals:

* Given a list of IP addresses, maintain communicative cluster capable of
  sending and receiving messages between members
* Maximize ability to maintain cluster with various commonplace network
  conditions:
  * dynamic IP address
  * periodic temporary connection loss
  * long term connection loss
  * hardware failure
* Ensure attack surface is minimized to third parties
  * nodes must allow connection from the internet due to changing IP addresses
  * randomly calculated rendezvous windows
