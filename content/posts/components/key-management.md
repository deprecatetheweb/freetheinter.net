---
title: "Key Management"
date: 2022-12-09
draft: false
showtoc: true
tocopen: true
weight: 55
tags:
- component

---

Cryptographic keys are used at every interface - between nodes to establish
identities, to decrypt received data, to encrypt data before transmission,
to encrypt data before storage, to decrypt data read from storage, etc.

It's critical that all other components are properly structured around
safe key management, so this will be one of the first components
developed.

# Key generation

Every user generates a root key that serves as the ultimate recovery key, which 
is used to bootstrap their cluster. This key is only online for setup or
recovery.

The root key is used to generate a deterministic set of derivative keys that
will serve as the primary key for nodes.

Any parent key can be used to invalidate any derivative key, and can
decrypt any data encrypted by any derivative key.

This means that

* data on any node can be recovered as long as the root key is not lost
* if a child node is compromised, a parent node can invalidate its key or
  recover its data, if accessible
* parent nodes can instruct child nodes to rekey at any time
* any node can rekey itself at any time
* if a peer node of the same cluster rekeys, peers will
  not be able to predict the new private key, but will be able to validate that
  the new public key is from the same parent

Nodes generate non-parent-derivable keys only for transient data in flight or
system-level data (e.g. cache) that is not necessarily relevant to data
recovery.

A root rekey can be performed in order to totally replace the master private
key chain (and public key chain). This will rotate the encryption of all data
in the cluster, and require reestablishment of identity
between all trusted clusters.

# Key exchange

## Between people

In-person key exchange is the fundamental activity that gives rise to a
stronger and safer trust model compared to legacy systems. Physical
presence allows the robust establishment of identity and connectivity
in a way that is impossible for non-person centralized entities.

This approach removes the need for centralized certificate authorities which
are required in legacy schemes.

## Between nodes

When nodes establish a connection, they must verify the identity of the other
node. They do this by validating that some public key of the other
node has been signed by some non-revoked key in its cluster.

Parent nodes generate derivative private keys and send them to derivative nodes
for bootstrapping.

Parent nodes send derivative nodes a list of their master public keys so they
can always identify themselves to the derivative node, even upon rekeying.

Nodes of a cluster send their master public key chain to the nodes of trusted
clusters in order to establish identity.

Nodes in a cluster distribute the master public key chain of other clusters to
all other nodes.

This means that

* if two nodes in a cluster have connected, all other nodes in the cluster will
  recognize each other in the future
* before sending data to a node, it is encrypted using that node's public key,
  and signed with its own private key

# Key use

1. signing data
   * all messages
2. encrypting symmetric key
   * data encryption before storage using own key
   * data encryption before transmission using others key
3. decrypting symmetric key
   * data dencryption after reading from storage using own key
   * data dencryption after receiving transmissions using others key
4. identity establishment
   * own cluster nodes
   * trusted cluster nodes
