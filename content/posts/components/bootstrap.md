---
title: "Project Bootstrap"
date: 2022-12-11
draft: false
showtoc: false
tocopen: true
weight: 50

---

The first goal of this project is to create
[the ideal social network]({{< ref "/posts/ideal-social-network.md" >}}).

[Nostr](https://github.com/nostr-protocol/nostr) seems like a great foundation!

See an overview of how nostr might be adapted for this project
[here]({{< ref "/posts/nostr.md" >}}).
