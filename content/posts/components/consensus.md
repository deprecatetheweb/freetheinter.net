---
title: Consensus Engine
date: 2022-07-01
draft: false
showtoc: true
tocopen: true
weight: 70
tags:
- component

---

If we're not going to rely on third parties to tell us what to think, we
need to build a system to help us arrive at consensus without them.

The overall approach here is for small groups of individuals to participate
in a process that allows them to achieve local consensus. With so many
independent consensus decisions being made, and with no ability to know the
results from any of them, mass propaganda will become much less effective.

Over time people will accumulate a wealth of consensus results that distill
the careful thoughts and discussions of those that they trust the most.

<!--more-->

This component features:

* consensus seeking for small groups
* predefined set of consensus mechanisms
* ability to define new mechanisms
* composition of consensus mechanisms

# Consensus in life

Consensus mechanisms are fundamental to human interaction. They're
how we make most important decisions.

Almost all human interaction can be viewed as a kind of consensus:

* chat
* handshake
* point-of-sale transaction
* the use of language
* SCOTUS
* passing legislation
* voting
* coin toss
* fist fight
* property lines
* traffic right-of-way
* bitcoin

These mechanisms have some similar properties:

* inputs
* mutually agreed-upon interaction pattern (shared state machine)
* consensus definition (end state)
* outputs

# Examples

## Normal conversation

One of the simplest consensus mechanisms is just a casual
exchange of messages without any particular order or restriction.

In this case the consensus mechanism's end state is each
participant signalling that it has ended.

Inputs:

* initial comment of the conversation
* privacy level
* accompanying data (title, URL, quote, essay, multimedia)
* list of tags (optional)

Interaction pattern:

* participant sends a message to the group
* participant replies to an existing message or sends a new message

Consensus definition:

* all participants signal that the conversation is over

Outputs:

* final set of all branches of conversation when consensus is met

## Debate

This type of consensus requires three inputs: a topic, a statement
about it, and a group of participants.
Outputs from the conversation are pairs of messages and their respective
consensus results.

Let's say that you want to see what your friends think
about some idea related to climate change policy. You initiate a
"topic discussion" conversation with the people you want to be
involved:

* select "policy debate" exchange type
* choose the topic "Climate change solution"
* comment "more nuclear power generation is good"

Each user is given the opportunity to agree, disagree, or simply
acknowledge each message.
The process then repeats for every subsequent rebuttal statement until all
participants have considered every message.

When the chosen threshold of participants hold the same opinion
about some message, consensus about it has been
achieved, and it is marked as "resolved", and added to the outputs of the
conversation.

After the exchange ends, because it is a public debate, the outputs are
shared with friends.

Rough process outline:

10 participant "policy debate" with 70% consensus threshold:
title - climage change solution
comment - more nuclear power generation is good
* initial agreement was 6/10 (no consensus)
  * rebuttal 1: nuclear is too expensive - vote 3/10 -> exported consensus
    * rebuttal 1 rebuttal 1: nuclear is not too expensive if it's a modern
      design - vote 9/10 -> exported consensus
  * rebuttal 2: it's not politically tenable to build new nuclear power
    plants - vote 8/9 -> exported consensus
    * no rebuttal
  * rebuttal 3: pizza tastes like fruit if it has fruit on it - vote 2/2
    (no consensus)
    * no rebuttal

In this case, the consensus was:

* nuclear power isn't too expensive
* modern designs of nuclear plants are cheaper
* it's not politically tenable to build nuclear power plants

There is one item that failed consensus and had three comments: "nucler power
is good". Statements or rebuttals that don't achieve consensus represent
opportunity for future exchanges. Maybe the idea was poorly stated,
some bad tradeoffs were made, it was simply incorrect, or was otherwise
controversial.

Someone can use this "nuclear power is good" comment (with failed consensus)
in a new exchange, set the consensus threshold, and comment
like "we will never use nuclear power for more than 10% of our energy", and
the cycle continues.

## Trade of goods

If you want to
send someone money for a one-time exchange, consensus involves:

* one party receives a bitcoin address and requested amount
* the requested amount is sent to the bitcoin address
* the sender of the money confirms that they received the product/service

## Shop of goods

The `Trade of goods` mechanism can be utilized multiple times in a more
complex form of consensus designed to offer an array of goods or services
for sale.

* consensus input is a list of items, amounts, and addresses
* consensus outputs are the agreed-upon list with one less item, and
  the consensus output of a `Trade of goods` consensus

## Elected servant

Outputs of this type of consensus are decisions about votes. In theory,
the elected official must use these as inputs in their decisions on a vote in
order for citizens to continue to trust them.

* voters get hard data about how the politician will use inputs to determine
  their vote.
* all "constituents" must be met in person and officially declared in advance
  of the election.
* users are a direct part of the candidacy. they are effectively being elected
  with the candidate.
* really just a small organizational change from the current standard of
  elected officials consulting advisors/lobbyists to guide their decisions. in
  this case, the advisors/lobbyists are the constituents, and the association
  is communicated openly.

## Maid

In order to automate some interactions, it's useful to interact with
a machine counterparty. The essential composition of the interaction will be
the same as interacting with real people, but the types of consensus
will differ.

Some examples:

* node controller
* cluster manager
* key generation
* acquaintance management
* dead man's switch

Anyone can summon maids in their own cluster and use them as needed for
any part of consensus.

Users establish consensus with virtual maids in order to
achieve some goal or generate some needed input.

## Other ideas

* Proposal for employment
* Law change
* Judgement
* Diary
* Grocery list
* Chatbot

# Composition of consensus channels

An important part of implementing consensus as a core feature is the ability
to compose it. This allows the chaining of multiple consensus results
together as inputs to others, and assembling arbitrarily complex interactions
between them.

With this approach all interactions are factored into the consensus channel
model, normalizing the interface.

## Dead man's switch

It's possible to implement a dead man's switch using a maid and consensus!

This is a very mechanical kind of interaction,
because it achieves consensus if and only
if it doesn't receive any message in some time interval.

The maid essentially wakes up every so often and checks to see if a message
has been sent to them by the participant. If the message hasn't been received,
consensus is achieved and the output is a message which is routed into
another consensus channel.

E.g. I create a dead man's switch maid, and then tell it to send the consensus
message to my custody maid.

## Custody maid

A custody maid's goal is to act if you are ever taken into someone else's
custody or otherwise incapacitated.

This is a special "contingency consensus" where the
inputs are a dead man's switch signal, you, a maid,
and messages sent between the two.

The messages are used to establish contingency plans with the maid that will
govern its actions when triggered.

When the dead man's switch output is received, the maid may send messages
to some list of your friends. Based on their responses, it could distribute
your will, send further instructions to your friends, distribute money,
hire an attorney, or anything else.

## Insurance cooperative

A group of ten or twenty friends can pool their money to have enough
resources to cover their car insurance.

A group of ten or twenty groups of friends can pool their money to cover
their health insurance.
