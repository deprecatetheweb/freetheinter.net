---
title: FAQ
date: 2022-09-17
draft: false
tags:
- faq
- advocacy
- meta
- cta
---

## _What it this?_

<!--more-->

This project is intended to accelerate the creation and adoption of software
which will return power to where it belongs - individuals.

Imagine a social network where the only content you ever see is shared by
people you know and trust. No Ads. No profiteering content
algorithms. No divisive manipulation. Truth thrives and spreads, while lies
and propaganda are organically filtered out.

We've described
[the ideal social network]({{< ref "/posts/ideal-social-network.md" >}}),
and plans for its development are taking shape.

## _Why?_

We have, as a society, lost the ability to determine what is true. We're being
manipulated and turned against one another by powerful centralized entities
that we cannot hope to overcome without better organization. This organization
will be realized only after we start using social networks that give us as
individuals all of the control.

The internet allows us to connect in uncountably many ways, but
the vast majority of connections are to untrustworthy centralized entities.

We must find better ways to connect and collaborate.

## _Is this like bitcoin and blockchain?_

On a technical level, not really. Interpersonal social interactions have no
need for global consensus, transaction validation, token distribution,
or proof of work, so there is no blockchain.

But in spirit, they have much in common:

* inspired by centralized corruption, manipulation, and similar dysfunction
* reimplementations of old systems using decentralization and
  a user-focused trust model
* uncompromising data security
* designed to establish consensus

While bitcoin is the decentralized instrumentation of currency, humans
also need a decentralized instrumentation of information sharing. This can
be leveraged into the decentralized instrumentation of decision making.

Bitcoin is designed to create _global_ consensus about asset ownership.
This project's goal is to create _local_ consensus about topics that are
relevant and important to individuals.

## _How will this amass enough users to be useful?_

Because the functional units in
[the ideal social network]({{< ref "/posts/ideal-social-network.md" >}}),
are small groups of people, massive numbers
of users aren't required for a compelling experience.

The scale at which most people function socially is relatively small.
So in order
to empower _most people_, the system should be useful for small groups.
This means that, in contrast to for-profit centralized services that require
ad revenue and massive network effects, it's not necessary for all of one's
aquaintances to be using it. Individuals can just find a small group of
trusted friends to use the system in order to extract utility.

Although most of these small groups will never connect directly, eventually
they will connect _transitively_, effectively allowing the spread of information
across society, but without the dangerous exposure to centralized manipulators.

## _What will prevent the system from becoming too complicated?_

Social networks created by centralized systems are at a
distinct _disadvantage_ regarding systemic complexity. Their
implementation must serve two masters - users and owners - so the user
experience inevitably suffers when there is a conflict between utility and 
profit.

Additionally, centralized systems have to scale to massive numbers of users
in order to be profitable and effective. This is an additional distraction
from simply serving the user, and contributes to complexity.

In summary - the kind of system that only serves the needs of users will be
much simpler than these chimeric behemoths peddled by billionaire enterprises.

## _Infrastructure is difficult to maintain. How can everyone to run their own server software?_

Simplicity is the key. The agent software must have minimized state. Only
a few basic easily testable capabilities are required for a node to operate.

This is the main focus of the project right now - finding a design that allows
agents to meet the necessary simplicity, extensibility,
security, reliability, durability, and availability requirements.

(see the previous question as well)
