---
title: "About"
date: 2022-09-18
draft: false
searchHidden: true
robotsNoIndex: true
weight: 5

---

This project aims to defeat manipulation from centralized entities by
popularizing systems that put individuals in control of the spread of
information.

This project is currently pursuing these activities:

* blog about ideas related to the project to get attention.
* collect resources to acclerate development.
* break work into tickets and start working on them.

For more, please see [the faq]({{< ref "/posts/faq.md" >}}).
