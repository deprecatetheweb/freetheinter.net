---
date: 2022-11-27
draft: false
title: The Ideal Social Network
weight: 1
tags:
- personalsocialinfrastructure
- psi
- system
- design
- theory

---

In order for individuals to win the information war, they must be:

* resistant to manipulation by centralized entities, and
* able to safely and easily coordinate with those they trust

For this to be the case, the end-user features of an ideal social network are
assumed to be:

* data is only accessible to the intended recipients
* users only see content shared directly by people they deem trustworthy
* data is safe and accessible to the owner
* service is reliable

One _potentially_ feasible approach is outlined here.

# Overview

All self-interested centralized entities eventually make decisions that
favor themselves to the detriment of others.
As a result, it's unsafe to rely on information services controlled
by centralized entities.

Every individual should have sole control over their own social infrastructure
in order to establish themselves as equal peers to every other entity.
This requires individuals to host their own services, which in turn requires
minimizing the cost and complexity of hosting.

The system described here is a low-peer decentralized message exchange
platform with all design decisions made in the interest of the
individual user's independence from third party manipulation.

<!--more-->

This is interpreted to require:

* zero-config self-hosted infrastructure
* strong cryptographic primitives for security and privacy
* direct connections only to those we know in real life
* redundant data and service layer
* composable conversational consensus primitives

# User experience

1. Users install decentralized agents on their phone, PC, router, etc.
2. Individuals meet in person to establish connectivity and identities.
3. Users broadcast messages to and receive messages from those that they have
   connected with.
4. Messages can be simple communication, or follow more complex rules for
   particular purposes such as debate, discussion, announcement, invitation,
   etc.
5. A rich set of consensus methods helps users share, process, and act on
   information.
6. Information filters through our society organically, is more resistant
   to interference, and is refined into common truths that are
   widely accepted.

# Road to Development

There aren't any systems that work like this yet, so the first step is to
plan the development of such a system, and then build it.

This project is currently organized into several
[components](/posts/components/),
which are planned to be implemented in sequence.
