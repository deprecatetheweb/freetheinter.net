---
title: "Building on nostr"
date: 2022-12-23
draft: false
showtoc: false
tocopen: true
weight: 1

---

[nostr](https://github.com/nostr-protocol/nostr) is intended to be:

> The simplest open protocol that is able to create a censorship-resistant
> global "social" network once and for all.

This looks like a promising foundation for
[the ideal social network]({{< ref "/posts/ideal-social-network.md" >}})!

# How nostr fits

* easy to extend
* public key identities
* key exchange happens outside of the protocol
* simple client/relay architecture
* simple message format
* relays and clients for major platforms
* seed phrase key generation and key paths
  ([NIP-06](https://github.com/nostr-protocol/nips/blob/master/06.md))

# Concerns about nostr

Mass adoption of nostr in its current form may not result in a
decentralized architecture.

* relays are controlled by untrusted entities, creating a power imbalance
* relays may become expensive to operate, encouraging rent-seeking and
  increasing centralization
* relies on DNS registrars
* relies on TLS certificate authorities

# Adapting nostr

For nostr to qualify as
[the ideal social network]({{< ref "/posts/ideal-social-network.md" >}}),
following changes can be implemented:

* users run their own relays, giving them derivative keys which they use to
  encrypt all data
* users only connect to their own relays, which in turn only connect to the
  relays of their direct contacts

This will ensure all users of nostr are equals, keep the cost of relays low,
remove the use of DNS and TLS, and allow secure communication
between relays and clients (rather than only clients and clients).

# Implications

If this was adopted, the barrier to using nostr would be much higher, because
relays are much more difficult to run than clients.

This means the popularity of nostr would depend on making it as easy as possible
to run a relay.

Since one of the main goals of this project is to implement
[the ideal social network]({{< ref "/posts/ideal-social-network.md" >}}),
we'll give it a try!
